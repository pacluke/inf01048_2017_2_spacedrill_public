import argparse


def parser() -> (argparse.Namespace, list):
    """
    Parses command line arguments.

    :return: a tuple containing parsed arguments and leftovers
    """
    p = argparse.ArgumentParser(prog='Spacedrill')
    mode_p = p.add_subparsers(dest='mode')
    mode_p.required = True
    p.add_argument('-p1', nargs=1, choices=['controller1', 'controller2', 'custom_bot', 'parasite_bot',
                                            'collector_bot'],
                   help='Selects player 1 controller, default is controller in controller1 folder')
    p.add_argument('-w1', nargs=1,
                   help='Selects player 1 weight\'s file, leave empty to generate a random number')
    p.add_argument('-p2', nargs=1, choices=['controller1', 'controller2', 'custom_bot', 'parasite_bot',
                                            'collector_bot'],
                   help='Selects player 2 controller, default is controller in controller2 folder')
    p.add_argument('-w2', nargs=1,
                   help='Selects player 1 weight\'s file, leave empty to generate a random number')
    mode_p.add_parser('learn',
                      help='Starts %(prog)s in learning mode. This mode does not render the game to your screen, '
                           'resulting in '
                           'faster learning.\n')
    mode_p.add_parser('learn_gui',
                      help='Starts %(prog)s in learning mode. This mode does render the game to your screen so you '
                           'can observe the learning process\n')
    mode_p.add_parser('evaluate',
                      help='Starts %(prog)s in evaluation mode. This mode runs your AI with the weights/parameters '
                           'passed as parameter \n')
    mode_p.add_parser('play',
                      help='Starts %(prog)s in playing mode. You can control each action of the car using the arrow '
                           'keys of your keyboard.\n')
    arguments, leftovers = p.parse_known_args()
    p.parse_args()
    return arguments, leftovers
