import pygame
import config
import pymunk
from pymunk import pygame_util
from pymunk.pygame_util import to_pygame
from pygame.color import THECOLORS
from pygame import transform
from pygame.gfxdraw import textured_polygon
from pygame import draw
from math import degrees
from pymunk import Vec2d


class GUI:
    # PyGame screen dimensions
    WIDTH = 1000
    HEIGHT = 700
    RESOLUTION = Vec2d(WIDTH, HEIGHT)
    FONT_SIZE = 20

    def __init__(self, simulation):
        pygame.init()
        self.simulation = simulation
        self.simulation_resolution = Vec2d(simulation.WIDTH, simulation.HEIGHT)
        self.screen = pygame.display.set_mode((self.WIDTH, self.HEIGHT),pygame.RESIZABLE)
        self.screen.set_alpha(None)
        pygame.display.set_caption('Spacedrill')
        self.draw_options = pygame_util.DrawOptions(self.screen)

        self.background = pygame.image.load('assets/background.jpg')
        self.background = transform.scale(self.background, (self.WIDTH, self.HEIGHT))

        self.asteroid_texture = pygame.image.load('assets/asteroid_texture.png')

        self.drill_1_img_original = pygame.image.load('assets/red_drill.png')
        self.drill_2_img_original = pygame.image.load('assets/blue_drill.png')
        new_size = (int(151/2.3), int(287/2.3))
        self.drill_1_img = pygame.transform.scale(self.drill_1_img_original, new_size)
        self.drill_2_img = pygame.transform.scale(self.drill_2_img_original, new_size)

        self.mothership_1_img = pygame.image.load('assets/red_mothership.png')
        self.mothership_2_img = pygame.image.load('assets/blue_mothership.png')

        # Font setup
        self.font = pygame.font.SysFont("monospace", self.FONT_SIZE)

        # Pygame general configurations
        pygame.RESIZABLE = True

        self.drill_1_drill_sprite = DrillSprite()
        self.drill_2_drill_sprite = DrillSprite()

    def draw(self):

        # Updates Asteroid color
        resource_drained = self.simulation.asteroid.resources_initial - self.simulation.asteroid.resources
        color_modifier = int((resource_drained/self.simulation.asteroid.resources_initial)*(255-THECOLORS['grey50'][0]))
        c = THECOLORS['grey50']
        self.simulation.asteroid.asteroid_shape.color = (min(c[0]+color_modifier, 255), c[1], c[2], c[3])

        # Draw background
        self.screen.fill(THECOLORS['black'])
        self.screen.blit(self.background, (0, 0))

        # Draw drills

        if self.simulation.drill_1.touching_asteroid or self.simulation.drill_1.touching_mothership:
            self.drill_1_drill_sprite.update()
        if self.simulation.drill_2.touching_asteroid or self.simulation.drill_2.touching_mothership:
            self.drill_2_drill_sprite.update()

        if self.simulation.drill_1.joint_blocked_time % 4 < 2:
            self.draw_drill(self.simulation.drill_1, self.drill_1_img, self.drill_1_drill_sprite)
        if self.simulation.drill_2.joint_blocked_time % 4 < 2:
            self.draw_drill(self.simulation.drill_2, self.drill_2_img, self.drill_2_drill_sprite)

        # Draw mothership

        self.draw_mothership(self.simulation.mother_ship_1, self.mothership_1_img)
        self.draw_mothership(self.simulation.mother_ship_2, self.mothership_2_img)

        # Draw textured asteroid
        asteroid_poly = self.simulation.asteroid.asteroid_shape.get_vertices()
        asteroid_poly_converted = [to_pygame(self.simulation.asteroid.local_to_world(poly), self.screen) for poly in
                                   asteroid_poly]

        asteroid_position = to_pygame(self.simulation.asteroid.position, self.screen)
        try:
            textured_polygon(self.screen, asteroid_poly_converted, self.asteroid_texture,  asteroid_position[0] - 1000,
                             1000-asteroid_position[1])
        except pygame.error:
            pass

        draw.lines(self.screen, THECOLORS['black'], True, asteroid_poly_converted, 4)

        # Draw hitboxes debug
        # for shape in self.simulation.shapes:
        #     self.draw_hitboxes(shape, THECOLORS['green'])

        # Draw text

        # Meteor resource text
        self.screen.blit(self.font.render(str(int(self.simulation.asteroid.resources)), False, THECOLORS['white']),
                         to_pygame(self.simulation.asteroid.position, self.screen))

        # Mothership P1 Text
        self.screen.blit(self.font.render(str(int(self.simulation.drill_1.resources)), False, THECOLORS['white']),
                         to_pygame(self.simulation.mother_ship_1.position, self.screen))

        # Mothership P2 Text
        self.screen.blit(self.font.render(str(int(self.simulation.drill_2.resources)), False, THECOLORS['white']),
                         to_pygame(self.simulation.mother_ship_2.position, self.screen))

        # METEOR INFO TEXT
        meteor_info_text = self.font.render("ASTEROID RESOURCES: %i" % self.simulation.asteroid.resources,
                                            False, THECOLORS['gray'])

        # P1 TEXT
        self.screen.blit(self.font.render("DRILL 1 RESOURCES: %i" % self.simulation.drill_1.resources,
                                          False, THECOLORS['red']), (0, 0))
        # self.screen.blit(self.font.render(
        #     "MOTHERSHIP 1 RESOURCES: %i" % self.simulation.mother_ship_1.resources, False, THECOLORS['red']),
        #     (0, self.FONT_SIZE)

        # P2 TEXT
        self.screen.blit(self.font.render("DRILL 2 RESOURCES: %i" % self.simulation.drill_2.resources,
                                          False, THECOLORS['blue']), (0, self.FONT_SIZE*4))
        # self.screen.blit(self.font.render(
        #     "MOTHERSHIP 2 RESOURCES: %i" % self.simulation.mother_ship_2.resources, False, THECOLORS['blue']),
        #     (0, self.FONT_SIZE*3))
        self.screen.blit(meteor_info_text, ((self.WIDTH/2) - (meteor_info_text.get_width()/2), 0))

        pygame.draw.rect(self.screen, THECOLORS["green"],
                         (1, self.FONT_SIZE + 4,
                          (self.simulation.drill_1.gasoline/self.simulation.drill_1.MAX_GASOLINE) * 100, 20))
        pygame.draw.rect(self.screen, THECOLORS['green'],
                         [1, (self.FONT_SIZE * 5) + 4,
                          (self.simulation.drill_2.gasoline / self.simulation.drill_1.MAX_GASOLINE) * 100, 20])

        pygame.display.flip()

    def draw_drill(self, drill_1, spaceship_image, drill_sprite):

        r = Vec2d(0, 70)
        r.rotate(drill_1.angle)
        drill_pos = to_pygame(drill_1.position + r, self.screen)
        temp_img = transform.rotate(drill_sprite.image, degrees(drill_1.angle))
        offset = Vec2d(temp_img.get_size()) / 2.
        drill_pos = drill_pos - offset
        self.screen.blit(temp_img, drill_pos)

        drill_pos = to_pygame(drill_1.position, self.screen)
        temp_img = transform.rotate(spaceship_image, degrees(drill_1.angle))
        offset = Vec2d(temp_img.get_size()) / 2.
        drill_pos = drill_pos - offset
        self.screen.blit(temp_img, drill_pos)

        pass

    def draw_hitboxes(self, shape, color):
        body_angle = shape.body.angle
        body_position = shape.body.position
        verts = []

        for v in shape.get_vertices():
            verts.append(v.rotated(body_angle) + body_position)

        verts = [pygame_util.to_pygame(v, self.screen) for v in verts]

        pygame.draw.lines(self.screen, color, True, verts)

    def draw_mothership(self, mothership, mothership_image):

        drill_pos = to_pygame(mothership.position, self.screen)
        temp_img = transform.rotate(mothership_image, degrees(mothership.angle))
        offset = Vec2d(temp_img.get_size()) / 2.
        drill_pos = drill_pos - offset
        self.screen.blit(temp_img, drill_pos)

    """
    def adjust_res(self, point) -> tuple:
        
        This function is used to allow this GUI run properly between multiple resolutions
        :param point: Point you want to adjust coordinates
        :return: 
        
        p = Vec2d(point)
        res = (p/self.simulation_resolution)*self.RESOLUTION
        res = int(res[0]), int(res[1])
        return res
    """


class DrillSprite(pygame.sprite.Sprite):
    def __init__(self):
        super(DrillSprite, self).__init__()
        self.images = []
        self.images.append(pygame.image.load('assets/drill/0.png'))
        self.images.append(pygame.image.load('assets/drill/1.png'))
        self.images.append(pygame.image.load('assets/drill/2.png'))
        self.images.append(pygame.image.load('assets/drill/3.png'))
        self.images.append(pygame.image.load('assets/drill/4.png'))

        for i in range(0, len(self.images)):
            width, height = self.images[i].get_size()
            self.images[i] = pygame.transform.scale(self.images[i], (int(width/2), int(height/2)))

        self.index = 0
        self.image = self.images[self.index]

    def update(self):
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]
